package ca.cyberseclab.avp.logviewer;

public class CanceledException extends Exception
{
	private static final long serialVersionUID = -8689949854901290383L;

	public CanceledException()
	{

	}

	public CanceledException(final String message)
	{
		super(message);
	}
}
