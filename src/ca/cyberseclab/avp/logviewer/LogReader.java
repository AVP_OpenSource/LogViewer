package ca.cyberseclab.avp.logviewer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javafx.stage.DirectoryChooser;

import org.apache.commons.io.FilenameUtils;

import ca.cyberseclab.avp.util.logger.LogViewer;

public class LogReader
{
	private final LogViewer logViewer;
	private final File initialDirectory;
	private boolean canceled = true;

	public LogReader(final LogViewer logViewer)
	{
		this.logViewer = logViewer;

		final DirectoryChooser directoryChooser = new DirectoryChooser();
		this.initialDirectory = directoryChooser.showDialog(null);
		if (this.initialDirectory != null)
		{

			try
			{
				this.readRecursively(this.initialDirectory);
				this.canceled = false;
			}
			catch (final IOException e)
			{
				// will be the same as if it was canceled
			}
		}
	}

	public boolean isCanceled()
	{
		return this.canceled;
	}

	private void readRecursively(final File currentFile) throws IOException
	{
		if (currentFile.isDirectory())
		{
			for (final File childFile : currentFile.listFiles())
			{
				this.readRecursively(childFile);
			}
		}
		else
		{
			this.readFile(currentFile);
		}
	}

	private void readFile(final File logFile) throws IOException
	{
		final String fileAbsolutePath = logFile.getAbsolutePath();
		final String fileExtension = FilenameUtils.getExtension(fileAbsolutePath);

		if (fileExtension.equals("txt"))
		{
			final String modelName = this.getModelNameFromAbsolutePath(fileAbsolutePath);

			final FileInputStream fileStream = new FileInputStream(logFile);
			final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
			String currentLine;

			while ((currentLine = bufferedReader.readLine()) != null)
			{
				if (!currentLine.isEmpty())
				{
					this.logViewer.addLog(currentLine, modelName);
				}
			}

			bufferedReader.close();
		}
	}

	private String getModelNameFromAbsolutePath(final String absolutePath)
	{
		final String initialDirectoryPath = this.initialDirectory.getAbsolutePath();

		// removes the base folder path from the specified file path
		// ex: C:/test/test2/test3.txt and C:/test gives test2/test3.txt 
		String result = absolutePath.substring(initialDirectoryPath.length() + 1, absolutePath.length());
		// replaces all the folder separator by dots
		result = result.replaceAll("\\Q" + File.separator + "\\E", ".");
		// removes the extension
		final int lastDotIndex = result.lastIndexOf(".");
		result = result.substring(0, lastDotIndex);

		return result;
	}
}
