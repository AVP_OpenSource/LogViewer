package ca.cyberseclab.avp.logviewer.model;

import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;
import ca.cyberseclab.avp.logviewer.model.filter.DateFilter;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.model.filter.NumberFilter;
import ca.cyberseclab.avp.logviewer.model.filter.StringFilter;
import ca.cyberseclab.avp.logviewer.tablelogview.filterfield.AbstractFilterField;
import ca.cyberseclab.avp.logviewer.tablelogview.filterfield.DateFilterField;
import ca.cyberseclab.avp.logviewer.tablelogview.filterfield.NumberFilterField;
import ca.cyberseclab.avp.logviewer.tablelogview.filterfield.StringFilterField;

public class FilterFactory
{
	public enum FilterType
	{
		STRING,
		NUMBER,
		DATE,
		ENUM
	}

	public static AbstractFilter createFilter(final FilterType filterType, final CompareOperator operator, final String value)
	{
		switch (filterType)
		{
		case DATE:
			return new DateFilter(operator, value);
		case ENUM:
		case NUMBER:
			return new NumberFilter(operator, value);
		case STRING:
			return new StringFilter(operator, value);
		default:
			// TODISCUSS ?
			return null;
		}
	}

	public static FilteringExpression createEmptyFilteringExpression(final FilterType filterType)
	{
		return new FilteringExpression(FilterFactory.createFilter(filterType, CompareOperator.NONE, ""));
	}

	// TODO move this
	public static AbstractFilterField createFilterField(final FilterType filterType)
	{
		switch (filterType)
		{
		case DATE:
			return new DateFilterField();
		case ENUM:
		case NUMBER:
			return new NumberFilterField();
		case STRING:
			return new StringFilterField();
		default:
			// TODISCUSS ?
			return null;
		}
	}

	private FilterFactory()
	{
	}
}
