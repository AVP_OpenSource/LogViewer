package ca.cyberseclab.avp.logviewer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Model
{
	private final ArrayList<ModelListener> listeners = Lists.newArrayList();

	private final List<ObservableLog> itemsAll = Lists.newArrayList();
	private final List<ObservableLog> itemsShowing = Lists.newArrayList();

	private final Map<String, FilteringExpression> filters = Maps.newHashMap();
	{
		for (final String columnName : ObservableLog.columnTypes.keySet())
		{
			this.setEmptyFilterForColumn(columnName);
		}
	}

	private final void setEmptyFilterForColumn(final String columnName)
	{
		this.filters.put(columnName, FilterFactory.createEmptyFilteringExpression(ObservableLog.columnTypes.get(columnName)));
	}

	public void addListener(final ModelListener listener)
	{
		this.listeners.add(listener);
	}

	public FilteringExpression getColumnFilteringExpression(final String columnName)
	{
		return this.filters.get(columnName);
	}

	public void setFilterForColumn(final FilteringExpression filter, final String columnName)
	{
		this.filters.put(columnName, filter);
		this.buildItemsShowing();
	}

	public void addLog(final ObservableLog log)
	{
		this.itemsAll.add(log);
		if (this.filterLog(log))
		{
			this.showLog(log);
		}
	}

	private void showLog(final ObservableLog log)
	{
		this.itemsShowing.add(log);
		this.notifyNewLog(log);
	}

	private boolean filterLog(final ObservableLog log)
	{
		for (final String columnName : this.filters.keySet())
		{
			final FilteringExpression currentExpression = this.filters.get(columnName);
			final String valueToFilter = log.get(columnName);
			if (!currentExpression.filter(valueToFilter))
			{
				return false;
			}
		}
		return true;
	}

	private void notifyNewLog(final ObservableLog log)
	{
		for (final ModelListener listener : this.listeners)
		{
			listener.newLogAdded(log);
		}
	}

	private void buildItemsShowing()
	{
		for (final ModelListener listener : this.listeners)
		{
			listener.logsCleared();
		}
		this.itemsShowing.clear();

		for (final ObservableLog currentLog : this.itemsAll)
		{
			if (this.filterLog(currentLog))
			{
				this.showLog(currentLog);
			}
		}
	}
}
