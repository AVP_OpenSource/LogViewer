package ca.cyberseclab.avp.logviewer.model.filter;

import java.util.EnumSet;
import java.util.Set;

import ca.cyberseclab.avp.logviewer.model.CompareOperator;

public class StringFilter extends AbstractFilter
{
	public static final Set<CompareOperator> VALID_OPERATORS = EnumSet.of(
					CompareOperator.NONE,
					CompareOperator.CONTAINS,
					CompareOperator.STARTS_WITH,
					CompareOperator.ENDS_WITH,
					CompareOperator.EQUALS,
					CompareOperator.NOT_EQUALS);

	public StringFilter(final CompareOperator operator, final String value)
	{
		super(operator, value);
	}

	@Override
	public boolean filter(final String valueToFilter)
	{
		switch (this.getOperator())
		{
		case CONTAINS:
			return valueToFilter.contains(this.getValue());
		case ENDS_WITH:
			return valueToFilter.endsWith(this.getValue());
		case EQUALS:
			return valueToFilter.equalsIgnoreCase(this.getValue());
		case NOT_EQUALS:
			return !valueToFilter.equalsIgnoreCase(this.getValue());
		case STARTS_WITH:
			return valueToFilter.startsWith(this.getValue());
		default:
			return true;
		}
	}
}
