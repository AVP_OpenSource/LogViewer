package ca.cyberseclab.avp.logviewer.model.filter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import ca.cyberseclab.avp.logviewer.model.CompareOperator;

public class DateFilter extends AbstractFilter
{
	public static final Set<CompareOperator> VALID_OPERATORS = EnumSet.of(
					CompareOperator.NONE,
					CompareOperator.AFTER,
					CompareOperator.AFTER_OR_ON,
					CompareOperator.BEFORE,
					CompareOperator.BEFORE_OR_ON,
					CompareOperator.EQUALS,
					CompareOperator.NOT_EQUALS);

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy MM dd H mm ss");

	public DateFilter(final CompareOperator operator, final String value)
	{
		super(operator, value);
	}

	@Override
	public boolean filter(final String valueToFilter)
	{
		try
		{
			final Date filterDate = DateFilter.DATE_FORMAT.parse(this.getValue());
			final Date valueDate = DateFilter.DATE_FORMAT.parse(valueToFilter);

			final boolean currentDateIsAfterFilter = valueDate.after(filterDate);
			final boolean currentDateIsEqualToFilter = valueDate.equals(filterDate);

			switch (this.getOperator())
			{
			case AFTER:
				return currentDateIsAfterFilter;
			case AFTER_OR_ON:
				return (currentDateIsAfterFilter && currentDateIsEqualToFilter);
			case BEFORE:
				return (!currentDateIsAfterFilter && !currentDateIsEqualToFilter);
			case BEFORE_OR_ON:
				return !currentDateIsAfterFilter;
			case EQUALS:
				return currentDateIsEqualToFilter;
			case NOT_EQUALS:
				return !currentDateIsEqualToFilter;
			default:
				break;
			}
		}
		catch (final ParseException e)
		{
			// could happen if field is empty, just continue
		}

		return true;
	}

}
