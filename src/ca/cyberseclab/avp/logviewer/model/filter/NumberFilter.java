package ca.cyberseclab.avp.logviewer.model.filter;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.Set;

import ca.cyberseclab.avp.logviewer.model.CompareOperator;

public class NumberFilter extends AbstractFilter
{
	public static final Set<CompareOperator> VALID_OPERATORS = EnumSet.of(
					CompareOperator.NONE,
					CompareOperator.GREATER_THAN,
					CompareOperator.GREATER_THAN_OR_EQUALS,
					CompareOperator.LESS_THAN,
					CompareOperator.LESS_THAN_OR_EQUALS,
					CompareOperator.EQUALS,
					CompareOperator.NOT_EQUALS);

	public NumberFilter(final CompareOperator operator, final String value)
	{
		super(operator, value);
	}

	@Override
	public boolean filter(final String valueToFilter)
	{
		if (this.getValue().isEmpty())
		{
			return true;
		}

		final BigDecimal filterNumber = new BigDecimal(this.getValue().trim());
		final BigDecimal valueNumber = new BigDecimal(valueToFilter.trim());

		final int compareToResult = valueNumber.compareTo(filterNumber);

		switch (this.getOperator())
		{
		case EQUALS:
			return compareToResult == 0;
		case GREATER_THAN:
			return compareToResult > 0;
		case GREATER_THAN_OR_EQUALS:
			return compareToResult >= 0;
		case LESS_THAN:
			return compareToResult < 0;
		case LESS_THAN_OR_EQUALS:
			return compareToResult <= 0;
		case NOT_EQUALS:
			return compareToResult != 0;
		default:
			return true;
		}
	}

}
