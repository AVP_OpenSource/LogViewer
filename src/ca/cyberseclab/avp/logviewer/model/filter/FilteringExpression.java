package ca.cyberseclab.avp.logviewer.model.filter;

import ca.cyberseclab.avp.logviewer.model.CompareOperator;

public class FilteringExpression
{
	// TODO change this whole class to work with a matrice of AND/OR filters
	private final AbstractFilter filter;

	public FilteringExpression(final AbstractFilter filter)
	{
		this.filter = filter;
	}

	public AbstractFilter getFilter()
	{
		return this.filter;
	}

	public boolean hasFilters()
	{
		return this.filter.getOperator() != CompareOperator.NONE;
	}

	public boolean filter(final String valueToFilter)
	{
		return this.filter.filter(valueToFilter);
	}

}
