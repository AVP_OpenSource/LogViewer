package ca.cyberseclab.avp.logviewer.model.filter;

import ca.cyberseclab.avp.logviewer.model.CompareOperator;

public abstract class AbstractFilter
{
	private final CompareOperator operator;
	private final String value;

	public AbstractFilter(final CompareOperator operator, final String value)
	{
		this.operator = operator;
		this.value = value;
	}

	public String getValue()
	{
		return this.value;
	}

	public CompareOperator getOperator()
	{
		return this.operator;
	}

	public abstract boolean filter(final String valueToFilter);
}
