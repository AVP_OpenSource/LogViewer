package ca.cyberseclab.avp.logviewer.model;

public enum CompareOperator
{
	NONE("None"),
	STARTS_WITH("Starts with"),
	ENDS_WITH("Ends with"),
	CONTAINS("Contains"),
	GREATER_THAN("Greater than"),
	LESS_THAN("Less than"),
	GREATER_THAN_OR_EQUALS("Greater than or equals"),
	LESS_THAN_OR_EQUALS("Less than or equals"),
	AFTER("After"),
	AFTER_OR_ON("After or on"),
	BEFORE("Before"),
	BEFORE_OR_ON("Before or on"),
	EQUALS("Equals"),
	NOT_EQUALS("Not equals");

	private String displayText;

	CompareOperator(final String displayText)
	{
		this.displayText = displayText;
	}

	@Override
	public String toString()
	{
		return this.displayText;
	}
}
