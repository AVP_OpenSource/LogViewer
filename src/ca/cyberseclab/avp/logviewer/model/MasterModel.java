package ca.cyberseclab.avp.logviewer.model;

import java.util.Map;

import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;

import com.google.common.collect.Maps;

public class MasterModel
{
	private final Map<String, Model> models = Maps.newHashMap();

	public void addModel(final String name)
	{
		this.models.put(name, new Model());
	}

	public boolean containsModel(final String name)
	{
		return this.models.containsKey(name);
	}

	public void addListenerToModel(final ModelListener listener, final String modelName)
	{
		this.models.get(modelName).addListener(listener);
	}

	public void addLog(final ObservableLog log, final String modelName)
	{
		this.models.get(modelName).addLog(log);
	}

	public FilterType getColumnType(final String columnName)
	{
		return ObservableLog.columnTypes.get(columnName);
	}

	public void setColumnFilteringExpressionForModel(final FilteringExpression filter, final String columnName, final String modelName)
	{
		this.models.get(modelName).setFilterForColumn(filter, columnName);
	}

	public FilteringExpression getColumnFilteringExpressionFromModel(final String columnName, final String modelName)
	{
		return this.models.get(modelName).getColumnFilteringExpression(columnName);
	}

}
