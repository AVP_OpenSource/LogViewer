package ca.cyberseclab.avp.logviewer.model;

import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;


public interface ModelListener
{
    public void newLogAdded(ObservableLog log);
    
    public void logsCleared();
}
