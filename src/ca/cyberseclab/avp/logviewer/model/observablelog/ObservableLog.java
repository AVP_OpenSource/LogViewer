/**
 * 
 */
package ca.cyberseclab.avp.logviewer.model.observablelog;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ch.qos.logback.classic.spi.ILoggingEvent;

import com.google.common.collect.Maps;

/**
 * @author sfrenette
 */
public class ObservableLog
{
	public static final Map<String, FilterType> columnTypes = Maps.newHashMap();
	static
	{
		ObservableLog.columnTypes.put(ObservableLog.ABSOLUTE_TIMESTAMP_NAME, FilterType.DATE);
		ObservableLog.columnTypes.put(ObservableLog.RELATIVE_TIMESTAMP_NAME, FilterType.NUMBER);
		ObservableLog.columnTypes.put(ObservableLog.THREAD_NAME, FilterType.STRING);
		ObservableLog.columnTypes.put(ObservableLog.LOGGER_NAME, FilterType.STRING);
		ObservableLog.columnTypes.put(ObservableLog.LEVEL_NAME, FilterType.STRING);
		ObservableLog.columnTypes.put(ObservableLog.MESSAGE_NAME, FilterType.STRING);
	}

	public static final String ABSOLUTE_TIMESTAMP_NAME = "absoluteTimestamp";
	public static final String RELATIVE_TIMESTAMP_NAME = "relativeTimestamp";
	public static final String THREAD_NAME = "threadName";
	public static final String LOGGER_NAME = "loggerName";
	public static final String LEVEL_NAME = "level";
	public static final String MESSAGE_NAME = "message";

	private final StringProperty absoluteTimestamp = new SimpleStringProperty(this, ObservableLog.ABSOLUTE_TIMESTAMP_NAME);
	private final StringProperty relativeTimestamp = new SimpleStringProperty(this, ObservableLog.RELATIVE_TIMESTAMP_NAME);
	private final StringProperty threadName = new SimpleStringProperty(this, ObservableLog.THREAD_NAME);
	private final StringProperty loggerName = new SimpleStringProperty(this, ObservableLog.LOGGER_NAME);
	private final StringProperty level = new SimpleStringProperty(this, ObservableLog.LEVEL_NAME);
	private final StringProperty message = new SimpleStringProperty(this, ObservableLog.MESSAGE_NAME);

	private final Map<String, StringProperty> properties = new HashMap<String, StringProperty>();
	{
		this.properties.put(ObservableLog.ABSOLUTE_TIMESTAMP_NAME, this.absoluteTimestamp);
		this.properties.put(ObservableLog.RELATIVE_TIMESTAMP_NAME, this.relativeTimestamp);
		this.properties.put(ObservableLog.THREAD_NAME, this.threadName);
		this.properties.put(ObservableLog.LOGGER_NAME, this.loggerName);
		this.properties.put(ObservableLog.LEVEL_NAME, this.level);
		this.properties.put(ObservableLog.MESSAGE_NAME, this.message);
	}

	public ObservableLog()
	{
	}

	public ObservableLog(final ILoggingEvent logEvent)
	{
		this.absoluteTimestamp.set(new Date(logEvent.getTimeStamp()).toString());

		final long relativeTimeInMS = logEvent.getTimeStamp() - logEvent.getLoggerContextVO().getBirthTime();
		this.relativeTimestamp.set(String.valueOf(relativeTimeInMS));

		this.threadName.set(logEvent.getThreadName());

		this.loggerName.set(logEvent.getLoggerName());

		this.level.set(logEvent.getLevel().toString());

		this.message.set(logEvent.getFormattedMessage());
	}

	public String get(final String name)
	{
		return this.properties.get(name).get();
	}

	public StringProperty getProperty(final String name)
	{
		return this.properties.get(name);
	}

	public String getAbsoluteTimestamp()
	{
		return this.absoluteTimestamp.get();
	}

	public StringProperty absoluteTimestampProperty()
	{
		return this.absoluteTimestamp;
	}

	public String getRelativeTimestamp()
	{
		return this.relativeTimestamp.get();
	}

	public StringProperty relativeTimestampProperty()
	{
		return this.relativeTimestamp;
	}

	public String getThreadName()
	{
		return this.threadName.get();
	}

	public StringProperty threadNameProperty()
	{
		return this.threadName;
	}

	public String getLoggerName()
	{
		return this.loggerName.get();
	}

	public StringProperty loggerNameProperty()
	{
		return this.loggerName;
	}

	public String getLevel()
	{
		return this.level.get();
	}

	public StringProperty levelProperty()
	{
		return this.level;
	}

	public String getMessage()
	{
		return this.message.get();
	}

	public StringProperty messageProperty()
	{
		return this.message;
	}
}
