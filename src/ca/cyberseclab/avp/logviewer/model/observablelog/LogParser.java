package ca.cyberseclab.avp.logviewer.model.observablelog;

public class LogParser
{
	private static final String LOG_COLUMN_DELIMITER = "|";

	public ObservableLog parse(final String textLog)
	{
		final String[] columns = textLog.split("\\Q" + LogParser.LOG_COLUMN_DELIMITER + "\\E");
		return new ObservableLogBuilder()
						.setAbsoluteTimestamp(columns[0])
						.setRelativeTimestamp(columns[1])
						.setThread(columns[2])
						.setLoggerName(columns[3])
						.setLevel(columns[4])
						.setMessage(columns[5])
						.build();
	}
}
