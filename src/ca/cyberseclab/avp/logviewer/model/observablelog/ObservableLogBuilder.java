package ca.cyberseclab.avp.logviewer.model.observablelog;

public class ObservableLogBuilder
{
	private String absoluteTimestamp = "";
	private String relativeTimestamp = "";
	private String thread = "";
	private String loggerName = "";
	private String level = "";
	private String message = "";

	public ObservableLog build()
	{
		final ObservableLog newLog = new ObservableLog();
		newLog.absoluteTimestampProperty().set(this.absoluteTimestamp);
		newLog.relativeTimestampProperty().set(this.relativeTimestamp);
		newLog.threadNameProperty().set(this.thread);
		newLog.loggerNameProperty().set(this.loggerName);
		newLog.levelProperty().set(this.level);
		newLog.messageProperty().set(this.message);

		return newLog;
	}

	public ObservableLogBuilder setAbsoluteTimestamp(final String absoluteTimestamp)
	{
		this.absoluteTimestamp = absoluteTimestamp;
		return this;
	}

	public ObservableLogBuilder setRelativeTimestamp(final String relativeTimestamp)
	{
		this.relativeTimestamp = relativeTimestamp;
		return this;
	}

	public ObservableLogBuilder setThread(final String thread)
	{
		this.thread = thread;
		return this;
	}

	public ObservableLogBuilder setLoggerName(final String loggerName)
	{
		this.loggerName = loggerName;
		return this;
	}

	public ObservableLogBuilder setLevel(final String level)
	{
		this.level = level;
		return this;
	}

	public ObservableLogBuilder setMessage(final String message)
	{
		this.message = message;
		return this;
	}

}
