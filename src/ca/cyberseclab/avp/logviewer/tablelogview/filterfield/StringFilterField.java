package ca.cyberseclab.avp.logviewer.tablelogview.filterfield;

import javafx.scene.control.TextField;
import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;
import ca.cyberseclab.avp.logviewer.model.filter.StringFilter;

public class StringFilterField extends AbstractFilterField
{
	private final TextField textField = new TextField();

	public StringFilterField()
	{
		super(StringFilter.VALID_OPERATORS);
		this.content.getChildren().add(this.textField);
	}

	@Override
	public AbstractFilter getFilter()
	{
		final StringFilter filter = new StringFilter(this.getOperator(), this.textField.getText());
		return filter;
	}

	@Override
	public void setValue(final String value)
	{
		this.textField.setText(value);
	}
}
