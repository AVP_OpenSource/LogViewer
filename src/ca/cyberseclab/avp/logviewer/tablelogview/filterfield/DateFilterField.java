package ca.cyberseclab.avp.logviewer.tablelogview.filterfield;

import java.text.ParseException;
import java.util.Calendar;

import jfxtras.labs.scene.control.CalendarTextField;
import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;
import ca.cyberseclab.avp.logviewer.model.filter.DateFilter;

public class DateFilterField extends AbstractFilterField
{
	private final CalendarTextField calendarTextField = new CalendarTextField();

	public DateFilterField()
	{
		super(DateFilter.VALID_OPERATORS);
		this.calendarTextField.setDateFormat(DateFilter.DATE_FORMAT);
		this.content.getChildren().add(this.calendarTextField);
	}

	@Override
	public AbstractFilter getFilter()
	{
		final String dateString = DateFilter.DATE_FORMAT.format(this.calendarTextField.getCalendar().getTime());
		return new DateFilter(this.getOperator(), dateString);
	}

	@Override
	public void setValue(final String value)
	{
		final Calendar calendar = Calendar.getInstance();
		try
		{
			calendar.setTime(DateFilter.DATE_FORMAT.parse(value));
			this.calendarTextField.setCalendar(calendar);
		}
		catch (final ParseException e)
		{
			this.calendarTextField.setCalendar(calendar);
		}
	}
}
