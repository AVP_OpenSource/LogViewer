package ca.cyberseclab.avp.logviewer.tablelogview.filterfield;

import java.util.Set;

import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import ca.cyberseclab.avp.logviewer.model.CompareOperator;
import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;

public abstract class AbstractFilterField extends VBox
{
	private final ChoiceBox<CompareOperator> choiceBox = new ChoiceBox<CompareOperator>();
	protected final VBox content = new VBox();

	public AbstractFilterField(final Set<CompareOperator> validOperators)
	{
		this.choiceBox.getItems().addAll(validOperators);
		this.choiceBox.getSelectionModel().selectFirst();

		this.getChildren().addAll(this.choiceBox, this.content);
		this.setStyles();
	}

	public abstract AbstractFilter getFilter();

	public abstract void setValue(final String value);

	public void reset()
	{
		this.choiceBox.getSelectionModel().select(CompareOperator.NONE);
		this.setValue("");
	}

	public void setOperator(final CompareOperator operator)
	{
		this.choiceBox.getSelectionModel().select(operator);
	}

	protected CompareOperator getOperator()
	{
		return this.choiceBox.getValue();
	}

	private void setStyles()
	{
		this.getStyleClass().add("filter-field");

		this.choiceBox.setMaxWidth(Double.MAX_VALUE);
		VBox.setVgrow(this.choiceBox, Priority.ALWAYS);

		VBox.setMargin(this, new Insets(0, 0, 10, 0));
	}
}
