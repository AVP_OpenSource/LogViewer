package ca.cyberseclab.avp.logviewer.tablelogview.filterfield;

import javafx.scene.control.TextField;
import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;
import ca.cyberseclab.avp.logviewer.model.filter.NumberFilter;

public class NumberFilterField extends AbstractFilterField
{
	private final TextField textField = new TextField();

	public NumberFilterField()
	{
		super(NumberFilter.VALID_OPERATORS);
		this.content.getChildren().add(this.textField);
	}

	@Override
	public AbstractFilter getFilter()
	{
		final NumberFilter filter = new NumberFilter(this.getOperator(), this.textField.getText());
		return filter;
	}

	@Override
	public void setValue(final String value)
	{
		this.textField.setText(value);
	}

}
