package ca.cyberseclab.avp.logviewer.tablelogview;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import ca.cyberseclab.avp.logviewer.CanceledException;
import ca.cyberseclab.avp.logviewer.Controller;
import ca.cyberseclab.avp.logviewer.UI.UI;
import ca.cyberseclab.avp.logviewer.model.ModelListener;
import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.tablelogview.event.TableFilteredEvent;

import com.sun.javafx.css.StyleManager;

public class MainWindow implements UI
{
	private final Controller controller;

	private final HBox container = new HBox();
	private final AnchorPane content = new AnchorPane();

	private final LogTreeView treeView;

	public MainWindow(final Stage primaryStage, final Controller controller)
	{
		Application.setUserAgentStylesheet(null);
		StyleManager.getInstance().addUserAgentStylesheet("/ca/thalesgroup/vl4m/logviewer/tablelogview/style/LogViewer.css");

		this.controller = controller;

		this.treeView = new LogTreeView();
		this.treeView.addOnTableFilteredEventHandler(this.onTableFiltered);
		this.treeView.setStyle("-fx-background-color: dimgray;");
		this.treeView.getSelectionModel().selectedItemProperty().addListener(this.onTreeItemSelected);

		HBox.setHgrow(this.content, Priority.ALWAYS);
		this.container.getChildren().addAll(this.treeView, this.content);

		primaryStage.setTitle("Log Viewer");
		primaryStage.setScene(new Scene(this.container, 900, 450));
		primaryStage.show();
	}

	private final EventHandler<TableFilteredEvent> onTableFiltered = new EventHandler<TableFilteredEvent>()
	{
		@Override
		public void handle(final TableFilteredEvent event)
		{
			MainWindow.this.controller.editFilterRequest(MainWindow.this, event.getColumn().getName(), event.getTable().getName());
		}
	};

	private final ChangeListener<TreeItem<String>> onTreeItemSelected = new ChangeListener<TreeItem<String>>()
	{
		@Override
		public void changed(final ObservableValue<? extends TreeItem<String>> item, final TreeItem<String> oldItem, final TreeItem<String> newItem)
		{
			final LogTreeItem newLogTreeItem = (LogTreeItem) newItem;
			if (newLogTreeItem.hasLogTable())
			{
				MainWindow.this.content.getChildren().clear();

				final FilterableTableView tableView = newLogTreeItem.getLogTable();
				AnchorPane.setTopAnchor(tableView, 0.0);
				AnchorPane.setLeftAnchor(tableView, 0.0);
				AnchorPane.setRightAnchor(tableView, 0.0);
				AnchorPane.setBottomAnchor(tableView, 0.0);
				MainWindow.this.content.getChildren().add(tableView);
			}
			else
			{
				MainWindow.this.content.getChildren().clear();
			}
		}
	};

	@Override
	public FilteringExpression askUserForFilterModification(final String columnName, final FilterType filterType, final FilteringExpression currentFilteringExpression, final String modelName) throws CanceledException
	{
		final FilterMenu filterMenu = new FilterMenu(filterType);
		filterMenu.loadFilteringExpression(currentFilteringExpression);
		filterMenu.show();

		if (filterMenu.isAccepted())
		{
			final FilteringExpression expression = filterMenu.getFilteringExpression();
			final FilterableTableView tableView = this.treeView.getTableForModel(modelName);
			tableView.getColumn(columnName).filteredProperty().set(expression.hasFilters());

			return expression;
		}
		throw new CanceledException();
	}

	@Override
	public ModelListener getModelListenerForModel(final String modelName)
	{
		return this.treeView.getTableForModel(modelName);
	}
}
