package ca.cyberseclab.avp.logviewer.tablelogview.event;

import javafx.event.Event;
import javafx.event.EventType;
import ca.cyberseclab.avp.logviewer.tablelogview.FilterableTableColumn;

public class ColumnFilteredEvent extends Event
{
	private static final long serialVersionUID = 7693994486973686150L;

	public static final EventType<ColumnFilteredEvent> COLUMN_FILTERED = new EventType<ColumnFilteredEvent>(Event.ANY, "COLUMN_FILTERED");

	private final FilterableTableColumn column;

	public ColumnFilteredEvent(final FilterableTableColumn column)
	{
		super(ColumnFilteredEvent.COLUMN_FILTERED);
		this.column = column;
	}

	public FilterableTableColumn getColumn()
	{
		return this.column;
	}
}
