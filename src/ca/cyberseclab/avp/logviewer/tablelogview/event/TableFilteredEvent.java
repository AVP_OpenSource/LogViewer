package ca.cyberseclab.avp.logviewer.tablelogview.event;

import javafx.event.Event;
import javafx.event.EventType;
import ca.cyberseclab.avp.logviewer.tablelogview.FilterableTableColumn;
import ca.cyberseclab.avp.logviewer.tablelogview.FilterableTableView;

public class TableFilteredEvent extends Event
{
	private static final long serialVersionUID = -2008871002313481838L;

	public static final EventType<TableFilteredEvent> TABLE_FILTERED = new EventType<TableFilteredEvent>(Event.ANY, "TABLE_FILTERED");

	private final FilterableTableColumn column;
	private final FilterableTableView table;

	public TableFilteredEvent(final FilterableTableColumn column, final FilterableTableView table)
	{
		super(TableFilteredEvent.TABLE_FILTERED);
		this.column = column;
		this.table = table;
	}

	public FilterableTableColumn getColumn()
	{
		return this.column;
	}

	public FilterableTableView getTable()
	{
		return this.table;
	}
}
