package ca.cyberseclab.avp.logviewer.tablelogview;

import javafx.scene.control.TreeItem;

public class LogTreeItem extends TreeItem<String>
{
	private FilterableTableView logTable = null;

	public LogTreeItem(final String name)
	{
		super(name);
	}

	public LogTreeItem(final String name, final FilterableTableView logTable)
	{
		super(name);
		this.logTable = logTable;
	}

	public boolean hasLogTable()
	{
		return this.logTable != null;
	}

	public void setLogTable(final FilterableTableView logTable)
	{
		this.logTable = logTable;
	}

	public FilterableTableView getLogTable()
	{
		return this.logTable;
	}

	public boolean contains(final String childName)
	{
		for (final TreeItem<String> currentChild : this.getChildren())
		{
			if (currentChild.getValue().equalsIgnoreCase(childName))
			{
				return true;
			}
		}
		return false;
	}

	public LogTreeItem getChild(final String childName)
	{
		for (final TreeItem<String> currentChild : this.getChildren())
		{
			if (currentChild.getValue().equalsIgnoreCase(childName))
			{
				return (LogTreeItem) currentChild;
			}
		}
		return null;
	}

	public void addChild(final LogTreeItem child)
	{
		LogTreeItem.this.getChildren().add(child);
	}
}
