package ca.cyberseclab.avp.logviewer.tablelogview;

import java.util.Map;
import java.util.NoSuchElementException;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ca.cyberseclab.avp.logviewer.model.ModelListener;
import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;
import ca.cyberseclab.avp.logviewer.tablelogview.event.ColumnFilteredEvent;
import ca.cyberseclab.avp.logviewer.tablelogview.event.TableFilteredEvent;

import com.google.common.collect.Maps;

public class FilterableTableView extends TableView<ObservableLog> implements ModelListener
{
	private final Map<String, FilterableTableColumn> columns = Maps.newHashMap();
	private final String name;

	public FilterableTableView(final String name)
	{
		this.name = name;

		final FilterableTableColumn absoluteTimeColumn = new FilterableTableColumn("Time", ObservableLog.ABSOLUTE_TIMESTAMP_NAME);
		absoluteTimeColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.ABSOLUTE_TIMESTAMP_NAME));

		final FilterableTableColumn relativeTimeColumn = new FilterableTableColumn("Relative Time", ObservableLog.RELATIVE_TIMESTAMP_NAME);
		relativeTimeColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.RELATIVE_TIMESTAMP_NAME));

		final FilterableTableColumn threadColumn = new FilterableTableColumn("Thread", ObservableLog.THREAD_NAME);
		threadColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.THREAD_NAME));

		final FilterableTableColumn loggerColumn = new FilterableTableColumn("Logger", ObservableLog.LOGGER_NAME);
		loggerColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.LOGGER_NAME));

		final FilterableTableColumn levelColumn = new FilterableTableColumn("Level", ObservableLog.LEVEL_NAME);
		levelColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.LEVEL_NAME));

		final FilterableTableColumn messageColumn = new FilterableTableColumn("Message", ObservableLog.MESSAGE_NAME);
		messageColumn.setCellValueFactory(new PropertyValueFactory<ObservableLog, String>(ObservableLog.MESSAGE_NAME));
		messageColumn.setPrefWidth(400);

		this.addColumn(absoluteTimeColumn);
		this.addColumn(relativeTimeColumn);
		this.addColumn(threadColumn);
		this.addColumn(loggerColumn);
		this.addColumn(levelColumn);
		this.addColumn(messageColumn);
	}

	private void addColumn(final FilterableTableColumn tableColumn)
	{
		this.getColumns().add(tableColumn);
		this.columns.put(tableColumn.getName(), tableColumn);
		tableColumn.addOnFilteredEventHandler(this.columnFilteredHandler);
	}

	private final EventHandler<ColumnFilteredEvent> columnFilteredHandler = new EventHandler<ColumnFilteredEvent>()
	{
		@Override
		public void handle(final ColumnFilteredEvent event)
		{
			final FilterableTableColumn column = event.getColumn();
			final FilterableTableView table = FilterableTableView.this;
			Event.fireEvent(table, new TableFilteredEvent(column, table));
		}
	};

	public void addOnFilteredEventHandler(final EventHandler<TableFilteredEvent> handler)
	{
		this.addEventHandler(TableFilteredEvent.TABLE_FILTERED, handler);
	}

	public FilterableTableColumn getColumn(final String columnName)
	{
		if (this.columns.containsKey(columnName))
		{
			return this.columns.get(columnName);
		}
		throw new NoSuchElementException();
	}

	public String getName()
	{
		return this.name;
	}

	@Override
	public void newLogAdded(final ObservableLog log)
	{
		this.getItems().add(log);
	}

	@Override
	public void logsCleared()
	{
		this.getItems().clear();
	}
}
