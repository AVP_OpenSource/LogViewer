package ca.cyberseclab.avp.logviewer.tablelogview;

import javafx.beans.property.BooleanProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseEvent;
import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;
import ca.cyberseclab.avp.logviewer.tablelogview.event.ColumnFilteredEvent;

public class FilterableTableColumn extends TableColumn<ObservableLog, String>
{
	private final FilterableTableColumnHeader columnHeader;
	private final String columnName;

	public FilterableTableColumn(final String columnTitle, final String columnName)
	{
		this.columnName = columnName;
		this.columnHeader = new FilterableTableColumnHeader(columnTitle);
		this.setGraphic(this.columnHeader);

		this.columnHeader.filterButton().addEventHandler(MouseEvent.MOUSE_CLICKED, new OnFilterButtonClicked());
	}

	public String getName()
	{
		return this.columnName;
	}

	public BooleanProperty filteredProperty()
	{
		return this.columnHeader.filteredProperty();
	}

	public void addOnFilteredEventHandler(final EventHandler<ColumnFilteredEvent> eventHandler)
	{
		this.addEventHandler(ColumnFilteredEvent.COLUMN_FILTERED, eventHandler);
	}

	private class OnFilterButtonClicked implements EventHandler<MouseEvent>
	{
		@Override
		public void handle(final MouseEvent event)
		{
			final FilterableTableColumn thisColumn = FilterableTableColumn.this;
			Event.fireEvent(thisColumn, new ColumnFilteredEvent(thisColumn));
		}
	}
}
