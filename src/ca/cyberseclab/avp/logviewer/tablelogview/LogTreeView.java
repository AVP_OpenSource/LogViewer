package ca.cyberseclab.avp.logviewer.tablelogview;

import java.util.HashMap;
import java.util.Map;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.TreeView;
import ca.cyberseclab.avp.logviewer.tablelogview.event.TableFilteredEvent;

public class LogTreeView extends TreeView<String>
{
	private static final String ROOT_LOGGER_NAME = org.slf4j.Logger.ROOT_LOGGER_NAME;

	private final Map<String, FilterableTableView> loggerTables = new HashMap<String, FilterableTableView>();

	public LogTreeView()
	{
		super(new LogTreeItem(LogTreeView.ROOT_LOGGER_NAME));

		final LogTreeItem root = ((LogTreeItem) this.getRoot());
		root.setExpanded(true);

		final FilterableTableView rootTable = new FilterableTableView(LogTreeView.ROOT_LOGGER_NAME);
		rootTable.addOnFilteredEventHandler(this.onTableFiltered);
		root.setLogTable(rootTable);

		this.loggerTables.put(LogTreeView.ROOT_LOGGER_NAME, rootTable);
	}

	public void addOnTableFilteredEventHandler(final EventHandler<TableFilteredEvent> handler)
	{
		this.addEventHandler(TableFilteredEvent.TABLE_FILTERED, handler);
	}

	public FilterableTableView getTableForModel(final String modelName)
	{
		if (!this.loggerTables.containsKey(modelName))
		{
			this.addLogTable(modelName, (LogTreeItem) this.getRoot(), modelName);
			this.waitFor(modelName);
		}
		return this.loggerTables.get(modelName);
	}

	private void waitFor(final String modelName)
	{
		while (this.loggerTables.get(modelName) == null)
		{
			try
			{
				Thread.sleep(200);
			}
			catch (final InterruptedException e)
			{
				// shouldn't be interrupted, continue to wait
			}
		}
	}

	private void addLogTable(final String modelName, final LogTreeItem currentNode, final String remainingChildrenNames)
	{
		// splits the current logger name in two, at the first dot occurence
		// Ex: services.call.5554 -> [0]: services [1]: call.5554
		final String[] nodes = remainingChildrenNames.split("\\.", 2);
		final String currentChildName = nodes[0];
		final LogTreeItem currentChild;

		// if the current child does not exist, we create it and add it
		if (!currentNode.contains(currentChildName))
		{
			currentChild = new LogTreeItem(currentChildName);
			currentNode.addChild(currentChild);
		}
		else
		{
			currentChild = currentNode.getChild(currentChildName);
		}

		// if length is greater than 1, it means there's still more children
		if (nodes.length > 1)
		{
			final String remainingChildren = nodes[1];
			this.addLogTable(modelName, currentChild, remainingChildren);
		}
		// we're at the last child, so we add the log table
		else
		{

			final FilterableTableView newLogTable = new FilterableTableView(modelName);
			newLogTable.addOnFilteredEventHandler(this.onTableFiltered);

			currentChild.setLogTable(newLogTable);
			LogTreeView.this.loggerTables.put(modelName, newLogTable);
		}
	}

	private final EventHandler<TableFilteredEvent> onTableFiltered = new EventHandler<TableFilteredEvent>()
	{
		@Override
		public void handle(final TableFilteredEvent event)
		{
			Event.fireEvent(LogTreeView.this, event);
		}
	};
}
