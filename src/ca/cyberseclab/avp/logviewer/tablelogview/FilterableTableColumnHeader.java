package ca.cyberseclab.avp.logviewer.tablelogview;

import javafx.beans.property.BooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;

public class FilterableTableColumnHeader extends BorderPane
{
	private final Label titleLabel = new Label();
	private final ToggleButton filterButton = new ToggleButton();

	public FilterableTableColumnHeader(final String headerTitle)
	{
		this.titleLabel.setText(headerTitle);

		this.setCenter(this.titleLabel);
		this.setRight(this.filterButton);

		this.setStyles();
	}

	// package access
	Label titleLabel()
	{
		return this.titleLabel;
	}

	// package access
	ToggleButton filterButton()
	{
		return this.filterButton;
	}

	public BooleanProperty filteredProperty()
	{
		return this.filterButton.selectedProperty();
	}

	private void setStyles()
	{
		BorderPane.setMargin(this.filterButton, new Insets(5, 5, 5, 0));

		this.getStyleClass().add("table-column-header");
		this.filterButton.getStyleClass().add("filter-button");
		this.titleLabel.getStyleClass().add("title");
	}

}
