package ca.cyberseclab.avp.logviewer.tablelogview;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.controlsfx.control.ButtonBar;
import org.controlsfx.control.ButtonBar.ButtonType;
import org.controlsfx.dialog.Dialog;

import ca.cyberseclab.avp.logviewer.model.FilterFactory;
import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ca.cyberseclab.avp.logviewer.model.filter.AbstractFilter;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.tablelogview.filterfield.AbstractFilterField;

public class FilterMenu extends Dialog
{
	private final BooleanProperty accepted = new SimpleBooleanProperty();

	private final VBox container = new VBox();
	private final VBox content = new VBox();

	private final ButtonBar buttonBar = new ButtonBar();
	private final Button saveButton = new Button("Save");
	private final Button cancelButton = new Button("Cancel");
	private final Button resetButton = new Button("Reset");

	// TODO Change to matrice AND/OR
	private final AbstractFilterField filterField;

	public FilterMenu(final FilterType filterType)
	{
		super(null, "Filter", false);
		this.filterField = FilterFactory.createFilterField(filterType);
		this.content.getChildren().add(this.filterField);

		this.setClosable(false);
		this.setIconifiable(false);
		this.setButtons();
		this.setStyles();

		this.container.getChildren().add(this.content);
		this.container.getChildren().add(this.buttonBar);
		this.setContent(this.container);
	}

	public void loadFilteringExpression(final FilteringExpression filteringExpression)
	{
		final AbstractFilter filter = filteringExpression.getFilter();
		this.filterField.setOperator(filter.getOperator());
		this.filterField.setValue(filter.getValue());
	}

	public FilteringExpression getFilteringExpression()
	{
		return new FilteringExpression(this.filterField.getFilter());
	}

	public BooleanProperty acceptedProperty()
	{
		return this.accepted;
	}

	public boolean isAccepted()
	{
		return this.accepted.get();
	}

	private class OnSaveClicked implements EventHandler<MouseEvent>
	{
		@Override
		public void handle(final MouseEvent event)
		{
			FilterMenu.this.accepted.set(true);
			FilterMenu.this.hide();
		}
	}

	private class OnCancelClicked implements EventHandler<MouseEvent>
	{
		@Override
		public void handle(final MouseEvent event)
		{
			FilterMenu.this.accepted.set(false);
			FilterMenu.this.hide();
		}
	}

	private class OnResetClicked implements EventHandler<MouseEvent>
	{
		@Override
		public void handle(final MouseEvent event)
		{
			FilterMenu.this.filterField.reset();
		}
	}

	private void setButtons()
	{
		this.saveButton.setDefaultButton(true);
		this.saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new OnSaveClicked());

		this.cancelButton.setCancelButton(true);
		this.cancelButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new OnCancelClicked());

		this.resetButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new OnResetClicked());

		this.buttonBar.addButton(this.saveButton, ButtonType.OK_DONE);
		this.buttonBar.addButton(this.cancelButton, ButtonType.CANCEL_CLOSE);
		this.buttonBar.addButton(this.resetButton, ButtonType.LEFT);
	}

	private void setStyles()
	{
		VBox.setVgrow(this.content, Priority.ALWAYS);
		this.container.getStyleClass().add("filter-menu");

		this.saveButton.getStyleClass().add("save-button");
		this.cancelButton.getStyleClass().add("cancel-button");
		this.resetButton.getStyleClass().add("reset-button");
	}

}
