package ca.cyberseclab.avp.logviewer.UI;

import ca.cyberseclab.avp.logviewer.CanceledException;
import ca.cyberseclab.avp.logviewer.model.ModelListener;
import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;

public interface UI
{
	public FilteringExpression askUserForFilterModification(final String columnName, final FilterType filterType, final FilteringExpression currentFilteringExpression, final String modelName) throws CanceledException;

	public ModelListener getModelListenerForModel(String modelName);
}
