package ca.cyberseclab.avp.logviewer;

import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import ca.cyberseclab.avp.core.LabManagerMaster;
import ca.cyberseclab.avp.logviewer.UI.UI;
import ca.cyberseclab.avp.logviewer.model.MasterModel;
import ca.cyberseclab.avp.logviewer.model.ModelListener;
import ca.cyberseclab.avp.logviewer.model.FilterFactory.FilterType;
import ca.cyberseclab.avp.logviewer.model.filter.FilteringExpression;
import ca.cyberseclab.avp.logviewer.model.observablelog.LogParser;
import ca.cyberseclab.avp.logviewer.model.observablelog.ObservableLog;
import ca.cyberseclab.avp.logviewer.tablelogview.MainWindow;
import ca.cyberseclab.avp.util.logger.LogViewer;
import ch.qos.logback.classic.spi.ILoggingEvent;

import com.google.common.collect.Lists;

public class Controller extends Application implements LogViewer
{
	private MasterModel model;
	private final List<UI> views = Lists.newArrayList();

	@Override
	public void start(final Stage stage)
	{
		this.model = new MasterModel();
		this.views.add(new MainWindow(stage, this));

		final LogReader logReader = new LogReader(this);
		if (logReader.isCanceled())
		{
			final LabManagerMaster labMaster = new LabManagerMaster("./scenarios/SampleScenario1.xml", this);
			final Thread thread = new Thread(labMaster);
			thread.start();
		}
	}

	@Override
	public void addLog(final ILoggingEvent log, final String loggerName)
	{
		final ObservableLog newLog = new ObservableLog(log);
		this.addLog(newLog, loggerName);
	}

	@Override
	public void addLog(final String log, final String loggerName)
	{
		final LogParser parser = new LogParser();
		this.addLog(parser.parse(log), loggerName);
	}

	private void addLog(final ObservableLog log, final String loggerName)
	{
		// the model name is the same as the logger name
		// if there's no model for this log yet, we create one and add listeners to it
		if (!this.model.containsModel(loggerName))
		{
			this.model.addModel(loggerName);
			for (final UI view : this.views)
			{
				final ModelListener currentViewListener = view.getModelListenerForModel(loggerName);
				this.model.addListenerToModel(currentViewListener, loggerName);
			}
		}
		this.model.addLog(log, loggerName);
	}

	public FilterType getColumnType(final String columnName)
	{
		return this.model.getColumnType(columnName);
	}

	public void editFilterRequest(final UI view, final String columnName, final String modelName)
	{
		final FilterType filterType = this.model.getColumnType(columnName);
		final FilteringExpression currentFilteringExpression = this.model.getColumnFilteringExpressionFromModel(columnName, modelName);

		try
		{
			final FilteringExpression newFilteringExpression = view.askUserForFilterModification(columnName, filterType, currentFilteringExpression, modelName);
			this.model.setColumnFilteringExpressionForModel(newFilteringExpression, columnName, modelName);
		}
		catch (final CanceledException e)
		{
			// user has canceled, nothing has changed 
		}
	}

	public FilteringExpression getColumnFilteringExpression(final String columnName, final String modelName)
	{
		return this.model.getColumnFilteringExpressionFromModel(columnName, modelName);
	}

	public static void main(final String[] args)
	{
		Application.launch(args);
	}
}
